#!/usr/bin/env bash

cat /usr/share/applications/*.desktop | grep -E "^Exec" | sed -e 's/Exec//' | sed -e 's/=//' | perl -pe 's/\s.+//' > ~/.cache/rofi-build-temp.druncache

# ~/.local/share/applications
# /usr/local/share/applications
