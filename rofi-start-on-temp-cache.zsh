#!/usr/bin/zsh

# FILES=/usr/bin/qpdfview /usr/bin/time-admin
FILES=$(cat ~/.cache/rofi-build-temp.druncache)
#FILES=$(cat ./rofi-build-from-zsh.druncache)


#FILE_INDEX=$(echo "$FILES" \
#  | sed -e "s#$HOME/Downloads/\(.*\)#\1#gi" \
#  | rofi -dmenu -i -p " Files in Downloads, latest first: " -format d)

FILE_INDEX=$(echo "$FILES" \
  | rofi -dmenu -i -p " Files in Cache, latest first: " -format d)

# If the user selected a file, open it up!
case $FILE_INDEX in
  -1) ;;
  "") ;;
  # *) zathura "$(echo "$FILES" | sed -n ${FILE_INDEX}p)" ;;
  *) "$(echo "$FILES" | sed -n ${FILE_INDEX}p)" ;;
  #*) exec "$(echo "$FILES")" ;;
esac
# TODO: with or without exec?

#~/scripts/my_script.sh | rofi -dmenu
#echo -e "Option #1\nOption #2\nOption #3" | rofi -dmenu
