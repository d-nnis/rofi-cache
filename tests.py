import unittest
import os

import build_rofi_cache

#import ipdb; ipdb.set_trace()

#from unittest import TestCase # TODO

CACHE_FILE = os.path.expanduser('~/.cache/rofi-build.druncache')


class SampleTest(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_nothing(self):
        pass


class CacheDir(unittest.TestCase):
    def test_find_cache_dir(self):
        pass

    def test_dir_writeable(self):
        pass


class CacheData(unittest.TestCase):
    """test case: reading desktop-files"""

    def helper_desktop_dirs(self):
        """helper to get desktop_dirs quickly"""
        desktop_dirs = build_rofi_cache.gather_dirs_desktop_files()
        return desktop_dirs

    def helper_desktop_files(self):
        """helper to get desktop_files quickly"""
        desktop_dirs = self.helper_desktop_dirs()
        desktop_files = build_rofi_cache.gather_desktop_files(desktop_dirs)
        return desktop_files

    def helper_execs(self):
        """helper to get execs quickly"""
        desktop_files = self.helper_desktop_files()
        execs = build_rofi_cache.gather_execs(desktop_files)
        return execs


    def test_find_dir(self):
        """find at least on dir with desktop files"""
        no_desktop_dirs = len(self.helper_desktop_dirs())
        # TODO: all entities are directories?
        # dir not found exception - has exception been raised?
        self.assertGreaterEqual(no_desktop_dirs, 1, f"Found {no_desktop_dirs} dirs for desktop files")

    def test_DirEntry_all_files(self):
        """every DirEntry is a file, nothing else"""
        # TODO: test of gather_desktop_files and extract_exec
        # are lumped together, should be tested isolated
        # FIXME: method name: desktop_files is no DirEnty list
        desktop_files = self.helper_desktop_files()
        no_dir_entries_not_files = 0
        for file_ in desktop_files:
            if not os.path.isfile(file_):
                no_dir_entries_not_files += 1

        self.assertEqual(no_dir_entries_not_files, 0, f"Number DirEntry that are not files: {no_dir_entries_not_files} ")

    def test_can_read_all_files(self):
        """drun: desktop-files"""
        desktop_files = self.helper_desktop_files()
        no_files_not_readable = 0
        for desktop_file in desktop_files:
            if not os.access(desktop_file, os.R_OK):
                no_files_not_readable += 1

        self.assertEqual(no_files_not_readable, 0, f"Files that are not readable: {no_files_not_readable}")

    def test_desktop_files_have_exec_string(self):
        """desktop-file has an exec-string"""
        execs = self.helper_execs()
        # TODO: iterate over object where filename and exec-string is contained to display more info
        for exec_string in execs:
            self.assertIsNotNone(exec_string, 'No exec-string')

        # TODO: test randomized sample of 3

    def test_only_unique_values(self):
        execs = self.helper_execs()
        execs = build_rofi_cache.only_unique_execs(execs)
        # list of times each value occurs
        count_exec = list(execs.count(exec) for exec in execs)
        # TODO: iterate over dict and print out the value which occurs more than once
        for c in count_exec:
            self.assertEqual(c, 1, 'One value occurs more than once')


    def test_exec_is_executable(self):
        """each exec-string of desktop-file represents an executable program within grasp of PATH"""
        # TODO: check: behavior of 1+ entries of a program in PATH
        execs = self.helper_execs()
        # alternative: path_env = os.path.expandvars('$PATH')
        path_env = os.environ['PATH']
        path = path_env.split(':')

        #for exec_ in execs:
        #    for p in path:
        #        exe = os.path.join(p, exec_)
        #        print("exe:", exe)
        #        executable = os.access(exe, os.X_OK)
        #        print("executable:", executable)
        ## equivalent to:

        for exec_ in execs:
            any_ = any(os.access(os.path.join(p, exec_), os.X_OK) for p in path)
            #print("full path:", os.path.join(p, exec_))
            #print("exec_:", exec_, ":", any_)
            self.assertTrue(any_, f'{exec_} not found or not executable')

        # FIXME: ? stops at first failure - get full list!


class CacheFile(unittest.TestCase):
    """test case: building cache file"""


    def test_cache_file_read(self):
        pass  # skip for now

        #content = build_rofi_cache.CacheFile.read(CACHE_FILE)
        #self.assertGreater(len(content), 0, "cache file has no content")

    def test_cache_file_write(self):
        """successfull cache_file write"""

        execs = CacheData().helper_execs()
        execs = build_rofi_cache.only_unique_execs(execs)
        CacheFile_inst = build_rofi_cache.CacheFile()
        # TODO: Test: Is error raised if CacheFile.append gets other
        # than data of type list?
        cache_file, cf_size = CacheFile_inst.write(execs, os.path.expanduser('~/.cache/rofi-build-testrun.druncache'))

        self.assertTrue(os.path.exists(cache_file) , f'{cache_file} does not exist')
        self.assertTrue(os.path.isfile(cache_file), f'{cache_file} is no file')
        self.assertEqual(os.path.getsize(cache_file), cf_size, f'{cache_file}\'s size diverts.')
        #self.assertAlmostEqual(os.path.getmtime(cache_file), time.now(), f'{cache_file}\'s date and time diverts.')
        # getctime or getmtime?

        # clean-up: delete cache_file
        print("Clean-up from test-run...")
        os.unlink(cache_file)
        self.assertFalse(os.path.exists(cache_file), f'{cache_file} could not be removed')


    def test_cache_file_exists(self):
        self.assertTrue(os.path.isfile(CACHE_FILE))

    def test_cache_file_writeable(self):
        self.assertTrue(os.access(CACHE_FILE, os.W_OK))

    def test_cache_file_write_success(self):
        """successfull cache_file write"""

        # instanciate the CacheData object and call method of that class
        execs = CacheData().helper_execs()
        execs = build_rofi_cache.only_unique_execs(execs)
        # check if folder is writeable
        cache_file, cf_size = build_rofi_cache.write_cache_file(execs, os.path.expanduser('~/.cache/rofi-build-testrun.druncache'))

        self.assertTrue(os.path.exists(cache_file) , f'{cache_file} does not exist')
        self.assertTrue(os.path.isfile(cache_file), f'{cache_file} is no file')
        self.assertEqual(os.path.getsize(cache_file), cf_size, f'{cache_file}\'s size diverts.')
        #self.assertAlmostEqual(os.path.getmtime(cache_file), time.now(), f'{cache_file}\'s date and time diverts.')
        # getctime or getmtime?

        # clean-up: delete cache_file
        print("Clean-up from test-run...")
        os.unlink(cache_file)
        self.assertFalse(os.path.exists(cache_file), f'{cache_file} could not be removed')


class RofiInstallation(unittest.TestCase):
    """test case: Rofi is installed"""
    def test_rofi_is_installed(self):
        """Rofi is installed, What OS?"""
        self.helper_get_os_release()

    def helper_get_os_release(self):
        """Get OS release"""
        # cat /etc/*-release | grep "^NAME"
        # TODO: much simpler: os.name() __AND__ sys.platform
        #import sys
        #os = sys.platform
        import re
        release_file = open('/etc/os-release', 'r')
        release_file_content = release_file.readlines()
        release_file.close()
        os = ""
        for line in release_file_content:
            match_name = re.search(r'^NAME', line)
            if match_name:
                match_debian = re.search(r'Debian', line)
                if match_debian:
                    print("OS is Debian!")
                else:
                    print("OS is unknown.")
                    print("OS Release is:", line)
                break

        #import subprocess
        #result = subprocess.run(['ls', '-l'], stdout=subprocess.PIPE)
        #output = result.stdout.decode('utf-8')
        #print("out:", output)

    def test_config_uses_cache_file(self):
        pass

    def test_rofi_can_start_programs(self):
        # rofi starts random sample
        pass


class RofiStartupScript(unittest.TestCase):
    """check startup script for correctness"""
    def test_rofi_startup_script(self):
        pass

# TODO: unittest can orchestrate external gui?

if __name__ == '__main__':
    unittest.main()

# development methodology
# TDD/ Double Loop-TDD, outside-inside. TDD is a programming/ software developing technique
# Test Coverage
# document the code (why, not what)
## The Unit-Test/Code Cycle
#Write and run the unit tests and see how they fail: no (correct) code. Fail indicates, that the test tests the right thing.
#Make a minimal code change to address the current test failure, make the test pass. (git commit)
#Refactor code/ design, i.e. remove duplicates: test for constant 1 and application also uses that constant, that is a duplication that justifies refactoring (having to stop cheating). (git commit)
#Start over with writing a new test.
#git commit after every success makes it easier to identify the cause of a regression.
#Before we start, we'll do a commit - always make sure you've got a commit of a working state before embarking on a refactor
# build cache for drun (desktop run mode from the freedesktop.org Desktop Entries)
# write a failing test for every test
## Double-Loop TDD
# FT: Top-level function or class (widget in a GUI, link on a webpage etc.)
# UT: design class or method that gets called by the top-level function - discovering that that class needs collaborator classes (outsourcing functionality)
# set-up mocks as collaborating classes: it is cheap to change mocks later on. Focus is therefore on interface and protocol.
# if happy and test passes move down the stack and work on developing the implementation of the underlying collaborating classes - moving through architectural layers and levels of abstraction.

## Refcatoring
# Red/ Green/ Refactor!

### Setup, Exercise, Assert - Cycle
## Setup (fixture?)
#List.objects.create(name='itemey 1')
#List.objects.create(name='itemey 2')
#
## Exercise
#response = self.client.get('/')
#
## Assert
#self.assertIn('itemey 1', response.content.decode())
#self.assertIn('itemey 2', response.content.decode())

## Code Smells

## LBYL vs EAFP
# Question of design and semantics (intelligibile): What is to be expected as the regular case? What is the common case?

# configurable
## update cache every x hours, e.g. with cron
# implement frequency of used programs? (weighted list)



# (check out external scripts)


#* CI, test coverage with coverage.py!
