#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
build cache for rofi app-launcher
"""

import re
import os

# locations of desktop-files according to freedesktop.org
# https://specifications.freedesktop.org/desktop-entry-spec/latest/
# ~/Schreibtisch?
# global CONSTANT vars, module non-public, underscore-prefix
_DIR = ['/usr/local/applications',
        '/usr/share/applications',
        '~/.local/share/applications']

_CACHE_FILE_PATH = os.path.expanduser('~/.cache/rofi-build.druncache')


def gather_dirs_desktop_files():
    """find directories with desktop files, keep onyl those"""
    # ~/Schreibtisch etc too?
    global _DIR
    # remove dir that does not exist, expand every path
    # expand dirs (~ --> /home/user) via list comprehension
    DIRS = [os.path.expanduser(d) for d in _DIR]
    dirs = list()
# TODO: try..except
    for dir_ in DIRS:
        dir_ = os.path.expanduser(dir_)
        if os.path.isdir(dir_):
            dirs.append(dir_)
    return dirs


# TODO: re-defining name from inner/ outer scope
def gather_desktop_files(desktop_dirs):
    """gather all desktop files """
    desktop_files = list()
    for desktop_dir in desktop_dirs:
        # TODO: log-management ("dir to read" etc.)
        #print("dir to read:", desktop_dir)
# os.listdir(I)
# or os.walk()
        #files = os.listdir(dir_)
## ScandirIterator (TODO: Iterator-Operations)
# TODO: try..except
        files = os.scandir(desktop_dir)
## single elements are of type DirEntry
        for file_ in files:  # get all dir-entries
            #files = os.path.isfile(file_)  # isfile works on DirEntry too (?)

            if file_.is_file():
                # TODO: include true positive to prove correct tests? Strategy to include tests of the unittest?
                #desktop_files.append(file_.path)
                if os.access(file_, os.R_OK):
                    # is readable
                    desktop_files.append(file_.path)
                else:
                    print("Is not readable, left out:", file_)
            else:
                print("Is no file, left out:", file_)
    # true positive:
    #desktop_files.append('/usr/share/applications/screensavers')
    return desktop_files


def extract_exec(desktop_file):
    """extract string after Exec=

    :desktop_file: a desktop file
    :returns: string after Exec=

    """
    df_handler = open(desktop_file, 'r')
    file_content = df_handler.readlines()
    df_handler.close()
    exec_string = None
# TODO: better use while?
    for line in file_content:
        #match_exec = re.search(r'^Exec=(.+)\s', line)  # TODO: works too, is non-greedy!?
        match_exec = re.search(r'^Exec=(.+?)\s', line)
        if match_exec:
            exec_string = match_exec.group(1)
            break

    return exec_string


def gather_execs(desktop_files):
    """gather all exec-strings"""

    execs = list()
    for desktop_file in desktop_files:
        exec_tmp = extract_exec(desktop_file)
        # testrun for true positive:
        #execs.append(exec_tmp)
        if exec_tmp:
            execs.append(exec_tmp)
        else:
            #raise Exception('NoExecException')
            print("has no exec, left out:", desktop_file)

    print("Execs length:", len(execs))
    return execs


def only_unique_execs(execs):
    """drop duplicate values via set-list-conversion"""
    execs = list(set(execs))
    return execs



# deprecated: refactored in CacheFile
def write_cache_file(execs, cache_file=None):
    if not cache_file:
        cache_file = _CACHE_FILE_PATH
    # from list to string
    # TODO: check on type(exec)
    execs_str = '\n'.join(execs)

    # TODO: file exists, file is non-zero
    cf_handler = open(cache_file, 'w')
    # TODO: disk is full
    # with..try..finally
# TODO: only to logfile?
    print("Write file", cache_file)
#try..except with
    cf_size = cf_handler.write(execs_str)  # returns bytes/ chars written
# finally
    cf_handler.close()
    return cache_file, cf_size


class CacheFile():
    """cache file object"""

    def __init__(self, content = None, filename = _CACHE_FILE_PATH):
        # TODO: check on type() = list
        #if not content:
        #    self._content = list()

        #if not type(content) == list:
        #    raise TypeError("wrong datatype, expecting list")

        try:
            self._content = content
            if not type(self._content) == list:
                raise TypeError("wrong or no datatype, expecting list")
        except TypeError as te:
            print(te)

        # but if self._content = None make it a list!

        self._filename = filename
        #self._content = ""

    @property
    def name(self):
        """set filename"""
        return self._filename

    @name.setter
    def name(self, filename):
        self._filename = filename

    def append(self, input_):
        """appends to content"""
        # check on type() = list
        try:
            self._content += input_
        except TypeError:
            print("appending of data impossible, wrong datatype")

    # "AttributeError: unreadable attribute" when trying
    # to read content. TODO: Is this sensible?
    append = property(fset=append)

    def read(self):
        #try..except..with opening and reading file
        # dummy data
        content = "i"
        return content

    # TODO: write only with content as object attribute
    def write(self, execs, cache_file=None):
        """write cache file"""
        if not cache_file:
            # cache_file = CACHE_FILE_PATH
            cache_file = self._filename
        # from list to string
        # TODO: check on type(exec)
        execs_str = '\n'.join(execs)

        # TODO: file exists, file is non-zero
        cf_handler = open(cache_file, 'w')
        # TODO: disk is full
        # with..try..finally
    # TODO: only to logfile?
        print("Write file", cache_file)
    #try..except with
        cf_size = cf_handler.write(execs_str)  # returns bytes/ chars written
    # finally
        cf_handler.close()
        return cache_file, cf_size

desktop_dirs = gather_dirs_desktop_files()
desktop_files = gather_desktop_files(desktop_dirs)
#extract_exec('/usr/share/applications/zathura.desktop')
#extract_exec('/home/dennish/.local/share/applications/mimeinfo.cache')
execs = gather_execs(desktop_files)
execs = only_unique_execs(execs)
#write_cache_file(execs)

RofiCache = CacheFile()
RofiCache.write(execs)


# TODO: transform to object-orientated design
# e.g. One class for open/ close cache_file
# another class for processing data
# --> All data are member variables
# coverage.py: how much code is tested?
#
# cache file with (Full) Name, Comment, Categories, Terminal false|true
## --> libreoffice-writer.desktop: Name necessary, exec with options as well
## --> "Files": nautilus
## show desktop file with executable and name
## does collect files that are (soft-)links?
# add custom programs via cache_file_custom
# organize cache file with frequency
